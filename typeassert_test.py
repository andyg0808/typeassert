import pytest
from importlib import reload

from typeassert import assertify, assertify_instance
from typing import List
import testmodule


def typed_func(thing: str) -> int:
    return len(thing)


def test_add_dict_assert():
    funcs = {"typed_func": typed_func}
    assertify(funcs)
    globals().update(funcs)
    assert typed_func("abc") == 3
    with pytest.raises(TypeError):
        typed_func([1, 2, 3])


def test_add_function_assert():
    def incr_func(thing: str) -> int:
        return len(thing) * 2

    assert incr_func("abc") == 6
    asserted = assertify(incr_func)
    assert asserted("abc") == 6
    with pytest.raises(TypeError):
        asserted([1, 2, 3])
        asserted(2)
        asserted({"a": 4})

    with pytest.raises(TypeError):
        incr_func(2)


def test_wrap_module():
    reload(testmodule)
    assert testmodule.make_eight(3) == 5
    with pytest.raises(TypeError):
        testmodule.make_six("abc")

    assertify(testmodule)

    assert testmodule.make_eight(3) == 5
    assert testmodule.testmoduleimport.make_six(6) == 0

    with pytest.raises(TypeError):
        testmodule.make_six("abc")
        testmodule.make_eight([])
        testmodule.testmoduleimport.make_six({})


class SampleClass:
    def __init__(self, dist: int = 0) -> None:
        self.dist = dist

    def incr(self) -> int:
        self.dist += 1
        return self.dist

    def add(self, amt: int) -> None:
        self.dist += amt


def test_wrap_class():
    i = SampleClass()
    i.incr()
    i.add(41)
    assert i.dist == 42

    with pytest.raises(TypeError):
        i.add([])

    # This is legal, although things will break later if we try to use this instance
    SampleClass([])

    assertify(SampleClass)

    with pytest.raises(TypeError):
        i.add([])

    with pytest.raises(TypeError):
        SampleClass({"a": 2})

    i.add(2)
    i.incr()
    assert i.dist == 45


class Haha:
    def __init__(self, thing: int) -> None:
        self.thing = thing

    def thing_up(self, length: list) -> None:
        self.thing += len(length)


def test_wrap_instance():
    h = Haha(3)
    h.thing_up([])
    h.thing_up([11])
    assert h.thing == 4
    with pytest.raises(TypeError):
        h.thing_up(43)
    assertify_instance(h)
    with pytest.raises(TypeError):
        h.thing_up({"a": 3})

    h.thing_up([1, 2, 3])
    assert h.thing == 7

    n = Haha(34)
    assert n.thing == 34

    with pytest.raises(TypeError):
        n.thing_up(34)

    Haha("acb")  # This would have raised a TypeError if it was wrapped.

def test_function_decorator():
    @assertify
    def decorated(t: int) -> int:
        return t + 2

    with pytest.raises(TypeError):
        decorated([])

    assert decorated(2) == 4

@assertify
class Decorated:
    def __init__(self, fish: int) -> None:
        pass

    def fishcount(self, fish: int) -> str:
        return f"There are {fish} fish"

def test_class_decorator():
    with pytest.raises(TypeError):
        Decorated([])

    i = Decorated(42)
    with pytest.raises(TypeError):
        i.fishcount('abc')

    assert i.fishcount(2) == 'There are 2 fish'
