import collections.abc
import inspect
import logging
import typing
from functools import singledispatch
from types import FunctionType, MethodType, ModuleType, new_class
from typing import (
    Any,
    Callable,
    List,
    Mapping,
    Optional,
    Type,
    TypeVar,
    Union,
    get_type_hints,
)

log = logging.getLogger("typeassert/assertify")
log.setLevel(logging.WARNING)

Asserted = Optional[Union[Callable, FunctionType, MethodType, ModuleType, Type]]

# Create new instance of list which serves as a marker for types which should
# not be checked.
Ignore = new_class("Ignore")

Conversions: Mapping[Any, Callable[[Any], bool]] = {
    List: lambda x: isinstance(x, list),
    Callable: callable,
    collections.abc.MutableMapping: lambda x: isinstance(x, dict),
    "Any": lambda x: False,
}


@singledispatch
def assertify(obj: object) -> Asserted:
    # If we don't have a special handler for a type, ignore it.
    pass


def assertify_instance(obj: Union[Type, ModuleType]) -> Asserted:
    return assertify_collection(obj)


@assertify.register(list)
def assertify_Names(names: List) -> Asserted:
    for name in names:
        assertify(name)
    return None


@assertify.register(dict)
def assertify_Dict(env: dict) -> None:
    for key, value in env.items():
        env[key] = assertify(value)
    return None


@assertify.register(ModuleType)
def assertify_Module(module: ModuleType) -> Asserted:
    return assertify_collection(module)


@assertify.register(FunctionType)
@assertify.register(MethodType)
def assertify_function(
    function: Callable, typetest: Callable[[Any, Type], bool] = None
) -> Asserted:
    """typetest: Callable A function which will decide whether a value is
    of the appropriate type. It will only be called if the value is
    not of the annotated type, and is passed the value as the first
    argument and the annotated type as the second. This is designed
    for situations where a type may be reflected purely in a typecode
    attribute on an object.
    """
    signature = inspect.signature(function)
    annotations = get_type_hints(function)
    parameters = signature.parameters

    def wrapper(*args: Any, **kwargs: Any) -> Any:
        log.debug("running wrapper...")
        for arg, param_name in zip(args, parameters):
            expected_type = annotations.get(param_name, inspect.Parameter.empty)
            if expected_type is not inspect.Parameter.empty:
                if inspect.getmodule(expected_type) is typing:
                    origin = getattr(
                        expected_type,
                        "__origin__",
                        getattr(expected_type, "_name", type(expected_type)),
                    )
                    concrete = Conversions.get(origin, None)
                    if concrete is not None:
                        assert concrete(arg)
                    else:
                        log.warning(
                            f"No concrete version of {expected_type}"
                            f"(searched for '{origin}' t{type(origin)}); skipping."
                        )
                elif isinstance(expected_type, TypeVar):
                    bound = expected_type.__bound__
                    if bound is not None:
                        assert isinstance(arg, bound)
                else:
                    log.info(
                        f"Checking {param_name}(value {arg}; type "
                        f"{type(arg)}) has type {expected_type} in call to "
                        f"{function.__name__}"
                    )
                    if isinstance(arg, expected_type):
                        pass
                    elif typetest is not None and typetest(arg, expected_type):
                        pass
                    else:
                        stack = inspect.stack()
                        if len(stack) > 1:
                            caller = stack[1].function
                        else:
                            caller = "unknown function"
                        raise TypeError(
                            f"{param_name} has unexpected type {type(arg)} "
                            f"(expected {expected_type}) in call to "
                            f"{function.__name__} from {caller}"
                        )
                        del stack

        return function(*args, **kwargs)

    return wrapper


@assertify.register(type)
def assertify_Class(cls: type) -> Asserted:
    return assertify_collection(cls)


def assertify_collection(blob: Union[Type, ModuleType]) -> Asserted:
    members = inspect.getmembers(blob, _member_check)
    log.debug("wrapping", members)
    for name, member in members:
        if name.startswith("_") and name != "__init__":
            continue
        res = assertify(member)
        if res is not None:
            log.debug("replacing", name, member, "with", res)
            setattr(blob, name, res)
        else:
            log.debug("no response")
    return blob


T = TypeVar("T")


def param_type(
    test: Callable[[Any], bool], param_type: Type
) -> Callable[[Any, Type], bool]:
    """
    Takes a test function and only applies it to parameters of param_type
    """

    def _tagtest(x: Any, t: Type) -> bool:
        return test(x) and param_type == t

    return _tagtest


def _member_check(x: Any) -> bool:
    if inspect.isbuiltin(x):
        return False
    return (
        inspect.ismodule(x)
        or inspect.isfunction(x)
        or inspect.isclass(x)
        or inspect.ismethod(x)
    )
