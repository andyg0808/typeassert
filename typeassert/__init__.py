from ._typeassert import assertify, assertify_instance, param_type, log

__version__ = "0.0.4"
