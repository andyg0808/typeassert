# Typeassert
Automatically and recursively wraps modules and functions with assertions to
check the types of the arguments. Uses PEP 484 type annotations as the source
of the typing information.

## Use
Import the `assertify` function from the typeassert module, and pass it a thing
to add assertions to. Modules, class declarations, and functions should all work.
If you want to add assertions to a single instance of a class, use the
`assertify_instance` function

The `assertify` function can also be used as a decorator on functions and
classes.
