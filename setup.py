# Based on sample setup.py at https://github.com/pypa/sampleproject/blob/master/setup.py
from os import path

from setuptools import find_packages, setup

import typeassert

basedir = path.abspath(path.dirname(__file__))
with open(path.join(basedir, "README.md")) as io:
    long_description = io.read()
setup(
    name="typeassert",
    version=typeassert.__version__,
    description="A sample Python project",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/andyg0808/typeassert",
    author="Andrew Gilbert",
    author_email="andrewg800@gmail.com",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Quality Assurance"
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
    ],
    keywords="development types testing",
    package_data={"typeassert": ["py.typed"]},
    packages=find_packages(),
    zip_safe=False,
    python_requires=">=3.7",
)
