from __future__ import annotations
import testmoduleimport
from testmoduleimport import make_six


def make_seven(v: int) -> int:
    six = testmoduleimport.make_six(v) + v
    seven = six + 1
    return seven - v


def make_eight(v: int) -> int:
    six = make_six(v) + v
    eight = six + 2
    return eight - v
